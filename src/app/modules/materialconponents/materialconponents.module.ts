import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { 
  MatSidenavModule,
  MatButtonModule,
  MatCardModule,
  MatListModule
  
  
}  from  '@angular/material';


@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    MatSidenavModule,
    MatButtonModule,
    MatCardModule,
    MatListModule
  ], 
  exports:[
    MatSidenavModule,
    MatButtonModule,
    MatCardModule,
    MatListModule
  ]

})
export class MaterialconponentsModule { }
