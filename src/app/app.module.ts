import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { environment } from  '../environments/environment';
import { MaterialconponentsModule }  from '../app/modules/materialconponents/materialconponents.module';
//firebase
import { AngularFireModule }  from  'angularfire2';
import { AngularFireDatabaseModule }  from 'angularfire2/database';
import { DashboardComponent }  from  './components/dashboard/dashboard.component'
import { ArticulosEncargoModule } from './components/articulos-encargo.module';

@NgModule({
  declarations: [
    AppComponent,
    DashboardComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    AngularFireModule.initializeApp(environment.firebaseConfig),
    AngularFireDatabaseModule,
    MaterialconponentsModule,
    ArticulosEncargoModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
