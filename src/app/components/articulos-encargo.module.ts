import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ArticulosEncargoRoutingModule } from './articulos-encargo-routing.module';
import { MaterialconponentsModule  } from  '../modules/materialconponents/materialconponents.module';
//graficos
import  { ChartsModule } from  'ng2-charts';
import { HomeComponent} from  './home/home.component'
import { ClienteComponent }  from  './cliente/cliente.component';
import { ArticuloComponent  } from  './articulo/articulo.component';
import { PedidoComponent} from  './pedido/pedido.component';
import { FabricaComponent } from  './fabrica/fabrica.component';
@NgModule({
  declarations: [
    HomeComponent,
    ClienteComponent,
    ArticuloComponent,
    PedidoComponent,
    FabricaComponent,
  
  ],
  imports: [
    CommonModule,
    ArticulosEncargoRoutingModule,
    MaterialconponentsModule,
    ChartsModule
  ]
})
export class ArticulosEncargoModule { }
