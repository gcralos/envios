import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ClienteComponent } from './cliente/cliente.component';
import { ArticuloComponent } from './articulo/articulo.component';
import { FabricaComponent } from './fabrica/fabrica.component';
import { PedidoComponent } from './pedido/pedido.component';
import { HomeComponent } from './home/home.component';
import { DashboardComponent } from './dashboard/dashboard.component';


const routes: Routes = [
  {path:'',redirectTo:'/home', pathMatch:'full'},
  {path:'', component:DashboardComponent,
      children:[
        { path:'home',component:HomeComponent },
        { path:'cliente',component:ClienteComponent },
        { path:'articulo',component:ArticuloComponent },
        { path:'fabrica',component:FabricaComponent },
        { path:'pedido',component:PedidoComponent }
      ]
  },
  
 
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ArticulosEncargoRoutingModule { }
